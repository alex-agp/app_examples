import json

from asgiref.sync import async_to_sync
from channels.layers import get_channel_layer

from apps.authentication.models import Company
from apps.notifications.models import Notifications
from apps.notifications.serializers import NotificationsSerializer


class NotificationComponent(object):
    def __init__(self):
        pass

    def create(self, name, description, recipient_id, sender_id, icon=None, url=None):
        notification = Notifications()
        notification.name = name
        notification.description = description
        notification.recipient_id = recipient_id
        notification.sender_id = sender_id
        notification.icon = icon
        notification.url = url
        notification.save()

        recipient = Company.objects.get(pk=recipient_id)
        self.send_last_sync_notifications(recipient)

        return notification

    def get_last_notifications(self, recipient):
        number_last_notifications = 5
        try:
            notifications = Notifications.objects.filter(recipient=recipient).order_by('-date_created')[:number_last_notifications]
            serializer = NotificationsSerializer(notifications, many=True)
        except Exception as e:
            message = {'details': str(e)}
            raise Exception(message)
        return serializer.data

    def mark_read(self, recipient):
        # no update(read=True) method as post_save signal won't working
        notifications = Notifications.objects.filter(recipient=recipient.company)
        for notification in notifications:
            notification.read = True
            notification.save()

        self.send_last_sync_notifications(recipient.company)  # update mostly for relative front times

    @staticmethod
    def send_last_sync_notifications(recipient):
        notifications = NotificationComponent().get_last_notifications(recipient)
        if not notifications:
            return

        channel_layer = get_channel_layer()
        async_to_sync(channel_layer.group_send)(
            'notif_company_{}'.format(recipient.id),
            {
                'type': 'notification.message',
                'notification_list': json.dumps(notifications)
            }
        )

