from apps.notifications.models import NotificationSetting
from apps.common.components.nearest_datetime.nearest_datetime import \
    NearestDateTime


class NotificationSettingComponent(object):
    def __init__(self, company):
        self.nearest_date_time = NearestDateTime()
        settings, created = NotificationSetting.objects.get_or_create(company=company)
        self.settings = settings

    def get(self):
        return self.settings
    
    def get_time_delay(self):
        frequency = self.settings.tender_activity_frequency

        return self.get_datetime_by_frequency(frequency)

    def get_chat_time_delay(self):
        frequency = self.settings.chat_frequency

        return self.get_datetime_by_frequency(frequency)

    def get_datetime_by_frequency(self, frequency):
        if frequency == NotificationSetting.Frequency.EVERY_HOUR:
            return self.nearest_date_time.get_hour()
        elif frequency == NotificationSetting.Frequency.EVERY_3HOURS:
            return self.nearest_date_time.get_3th_hour()
        elif frequency == NotificationSetting.Frequency.EVERY_24HOURS:
            return self.nearest_date_time.get_24th_hour()
        else:
            return None

    def is_enabled(self):
        return self.settings.tender_activity

    def is_chat_enabled(self):
        return self.settings.chat_message

    def is_email_enabled(self):
        return self.settings.notify_to_email
