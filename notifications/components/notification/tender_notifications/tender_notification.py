from abc import ABC, abstractmethod
from apps.notifications.components.notification.notification_setting_component import \
    NotificationSettingComponent


class TenderNotification(ABC):
    def __init__(self, user):
        super().__init__()
        self.settings = NotificationSettingComponent(user.company)

    @abstractmethod
    def send_notification(self):
        pass

    @abstractmethod
    def _send_notification_task(self):
        pass

    @abstractmethod
    def _send_email_notification_task(self):
        pass

    @abstractmethod
    def _set_url(self):
        pass


