from apps.components.emails import TenderNotificationsEmail
from apps.notifications.components.notification.tender_notifications.tender_notification import \
    TenderNotification

from apps.authentication.models import Company
from apps.notifications.models import Notifications
from apps.notifications.tasks import create_notification_task


class NotificationChatMessageReceived(TenderNotification):
    def __init__(self, contract, user, data=None):
        super().__init__(user)
        self.user = user
        self.contract = contract
        self.tender = contract.tender
        self.data = data

        self.name = 'Chat Message Received'
        self.description = '{} {} from {} sent a chat message "{}"'.format(
            data['sender'].first_name, data['sender'].last_name, data['sender'].company.name, data['message'])
        self.icon = Notifications.Icons.COMMENT

        self.recipient = user.company.id
        self.sender = data['sender'].id

        self._set_url()

    def send_notification(self):
        self._send_notification_task()

    def _send_notification_task(self):
        if self.settings.is_chat_enabled() is False:
            return

        create_notification_task.apply_async((
            self.name, self.description, self.recipient, self.sender, self.icon, self.url),
            eta=self.settings.get_chat_time_delay())

    def _send_email_notification_task(self):
        if self.settings.is_email_enabled() is False:
            return

        TenderNotificationsEmail(self.contract.tender, self.user, self.name, self.description).send(self.settings.get_time_delay())

    def _set_url(self):
        if self.user.company.type == Company.Types.BUYER:
            self.url = '/tenders/{}/locations/{}/dashboard/rounds/{}/suppliers/{}' \
                .format(self.tender.uid, self.contract.id, self.contract.current_round.round,
                        self._get_first_offer_id())
        elif self.user.company.type == Company.Types.SUPPLIER:
            self.url = '/supplier/tenders/{}/locations/{}/bid/{}'.format(self.tender.uid, self.contract.id,
                                                                     self._get_first_offer_id())
        else:
            self.url = None

    def _get_first_offer_id(self):
        try:
            return self.contract.current_round.offers.first().id
        except Exception as e:
            return None
