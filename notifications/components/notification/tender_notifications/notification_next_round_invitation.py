from apps.components.emails import TenderNotificationsEmail
from apps.notifications.components.notification.tender_notifications.tender_notification import \
    TenderNotification
from apps.notifications.models import Notifications
from apps.notifications.tasks import create_notification_task


class NotificationNextRoundInvitation(TenderNotification):
    def __init__(self, contract, user):
        super().__init__(user)
        self.user = user
        self.contract = contract
        self.tender = contract.tender

        self.name = 'Next Round Invitation'
        self.description = '{} {} from {} invite you to next round {} {} tender.'.format(
            self.tender.created_by.first_name, self.tender.created_by.last_name, self.tender.created_by.company.name,
            self.contract.location.iata, self.tender.name)
        self.icon = Notifications.Icons.CHECK

        self.recipient = user.company.id
        self.sender = self.tender.created_by.id

        self._set_url()

    def send_notification(self):
        self._send_notification_task()

    def _send_notification_task(self):
        if self.settings.is_enabled() is False:
            return

        create_notification_task.apply_async((
            self.name, self.description, self.recipient, self.sender, self.icon, self.url),
            eta=self.settings.get_time_delay())

    def _send_email_notification_task(self):
        if self.settings.is_email_enabled() is False:
            return

        TenderNotificationsEmail(self.contract.tender, self.user, self.name, self.description).send(self.settings.get_time_delay())

    def _set_url(self):
        self.url = '/supplier/tenders/{}/locations'.format(self.tender.uid)

