from apps.components.emails import TenderNotificationsEmail
from apps.notifications.components.notification.tender_notifications.tender_notification import \
    TenderNotification
from apps.notifications.models import Notifications
from apps.notifications.tasks import create_notification_task


class NotificationOfferSubmitted(TenderNotification):
    def __init__(self, contract, user):
        super().__init__(contract.tender.company.user)
        self.user = user
        self.contract = contract
        self.tender = contract.tender

        self.name = 'Bid Received'
        self.description = '{} has bid on {} in round {} of {} tender.'.format(
            user.company.name, self.contract.location.iata, self.contract.current_round.round, self.tender.name)
        self.icon = Notifications.Icons.DOLLAR

        self.recipient = self.tender.company.id
        self.sender = user.id

        self._set_url()

    def send_notification(self):
        self._send_notification_task()

    def _send_notification_task(self):
        if self.settings.is_enabled() is False:
            return

        create_notification_task.apply_async((
            self.name, self.description, self.recipient, self.sender, self.icon, self.url),
            eta=self.settings.get_time_delay())

    def _send_email_notification_task(self):
        if self.settings.is_email_enabled() is False:
            return

        TenderNotificationsEmail(self.contract.tender, self.tender.created_by, self.name, self.description).send(self.settings.get_time_delay())

    def _set_url(self):
        self.url = '/tenders/{}/locations/{}/dashboard'.format(self.tender.uid, self.contract.id)
