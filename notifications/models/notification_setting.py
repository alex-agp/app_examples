from django.db import models


class NotificationSetting(models.Model):
    class Frequency:
        IMMEDIATELY = 'immediately'
        EVERY_HOUR = 'every-hour'
        EVERY_3HOURS = 'every-3h'
        EVERY_24HOURS = 'every-24h'

        ENUM = [IMMEDIATELY, EVERY_HOUR, EVERY_3HOURS, EVERY_24HOURS]
        CHOICES = [(s, str(s).title()) for s in ENUM]

    company = models.ForeignKey('authentication.Company', on_delete=models.CASCADE)
    notify_to_email = models.BooleanField(default=True)
    chat_message = models.BooleanField(default=True)
    chat_frequency  = models.CharField(max_length=14, choices=Frequency.CHOICES, default=Frequency.IMMEDIATELY)
    tender_activity = models.BooleanField(default=True)
    tender_activity_frequency = models.CharField(max_length=14, choices=Frequency.CHOICES, default=Frequency.IMMEDIATELY)


