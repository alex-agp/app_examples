from django.db import models


class Notifications(models.Model):
    class Icons:
        DOLLAR = 'dollar-sign'
        COMMENT = 'comment-dots'
        AWARD = 'award'
        CHECK = 'check'
        BAN = 'ban'

    name = models.CharField(max_length=50)
    description = models.TextField(null=True)
    icon = models.CharField(max_length=20, null=True)
    url = models.CharField(max_length=255, null=True)
    date_created = models.DateTimeField(auto_now_add=True)
    read = models.BooleanField(default=False)
    recipient = models.ForeignKey('authentication.Company', related_name='notifications', null=True, on_delete=models.CASCADE)
    sender = models.ForeignKey('authentication.User', related_name='notifications', null=True, on_delete=models.CASCADE)

