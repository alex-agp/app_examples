from rest_framework import serializers

from apps.authentication.serializers import BriefUserSerializer
from apps.notifications.models import Notifications


class NotificationsSerializer(serializers.ModelSerializer):

    class Meta:
        model = Notifications
        fields = ('name', 'description', 'icon', 'url', 'read', 'date_created')
