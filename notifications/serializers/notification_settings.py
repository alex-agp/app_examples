from rest_framework import serializers

from apps.notifications.models import NotificationSetting


class NotificationSettingsSerializer(serializers.ModelSerializer):
    #chat_frequency = serializers.CharField(read_only=True)

    class Meta:
        model = NotificationSetting
        fields = (
            'notify_to_email',
            'chat_message',
            'chat_frequency',
            'tender_activity',
            'tender_activity_frequency'
        )
