from rest_framework import generics, pagination

from apps.components.django.permissions import IsAuthenticated
from apps.notifications.models import Notifications
from apps.notifications.serializers.notifications import \
    NotificationsSerializer


class NotificationsPagination(pagination.PageNumberPagination):
    page_size = 20
    page_size_query_param = 'page_size'


class NotificationsView(generics.ListAPIView):
    permission_classes = [IsAuthenticated]
    queryset = Notifications.objects.all()
    serializer_class = NotificationsSerializer
    pagination_class = NotificationsPagination

    def get_queryset(self):
        user = self.request.user
        queryset = Notifications.objects.filter(recipient=user.company).order_by('-id')

        return queryset

