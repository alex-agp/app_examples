from rest_framework import status, views
from rest_framework.response import Response

from apps.components.django.permissions.is_authenticated import \
    IsAuthenticated
from apps.notifications.components.notification.notification_component import \
    NotificationComponent


class NotificationsReadView(views.APIView):
    permission_classes = [IsAuthenticated]

    def put(self, request):
        NotificationComponent().mark_read(request.user)

        return Response(status=status.HTTP_200_OK)
