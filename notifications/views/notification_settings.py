from rest_framework import status, views
from rest_framework.response import Response

from apps.components.django.permissions import (IsAuthenticated,
                                                          IsManager)
from apps.notifications.components.notification.notification_setting_component import \
    NotificationSettingComponent
from apps.notifications.models import NotificationSetting
from apps.notifications.serializers import \
    NotificationSettingsSerializer


class NotificationSettingsView(views.APIView):
    permission_classes = [IsAuthenticated, IsManager]

    def get(self, request):
        company = self.request.user.company
        setting = NotificationSettingComponent(company=company).get()
        serializer = NotificationSettingsSerializer(setting)

        return Response(serializer.data, status=status.HTTP_200_OK)

    def put(self, request):
        company = self.request.user.company
        setting = NotificationSettingComponent(company=company).get()

        serializer = NotificationSettingsSerializer(setting, data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.save()

        return Response(serializer.data, status=status.HTTP_200_OK)





