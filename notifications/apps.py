from django.apps import AppConfig


class NotificationsConfig(AppConfig):
    name = 'apps.notifications'

    def ready(self):
        """Override this to put in:
            Users system checks
            Users signal registration
        """
        pass

