import json
from urllib.parse import parse_qs

from django.db.models.signals import post_save
from django.dispatch import receiver

from asgiref.sync import async_to_sync
from channels.consumer import AsyncConsumer
from channels.layers import get_channel_layer

from apps.authentication.components.json_token_auth import \
    JSONWebTokenAuthenticationValue
from apps.notifications.components.notification.notification_component import \
    NotificationComponent
from apps.notifications.models import Notifications

# @receiver(post_save, sender=Notifications)
# def signal_notification_save(sender, instance, created, **kwargs):
    # when only created new notification
    # if created:
    #     NotificationComponent().send_last_sync_notifications(instance.recipient)


class NotificationsConsumer(AsyncConsumer):

    async def websocket_connect(self, event):
        params = parse_qs(self.scope['query_string'].decode('utf8'))
        self.jwt_token = params['t'][0]
        self.auth_socket_user()

        if not self.user.is_authenticated:
             return

        self.room = 'notif_company_{}'.format(self.user.company.id)

        # Join room group
        await self.channel_layer.group_add(
            self.room,
            self.channel_name
        )

        await self.send({
            "type": "websocket.accept",
        })

        await self.send_notifications()

    async def send_notifications(self):
        notifications = NotificationComponent().get_last_notifications(self.user.company)

        await self.channel_layer.group_send(
            self.room,
            {
                'type': 'notification.message',
                'notification_list': json.dumps(notifications)
            }
        )

    async def notification_message(self, event):
        notifications = event['notification_list']

        # Send message to WebSocket
        await self.send({
            "type": "websocket.send",
            "text": notifications,
        })

    def auth_socket_user(self):
        self.user, jwt_value = JSONWebTokenAuthenticationValue().authenticate(self.jwt_token)
