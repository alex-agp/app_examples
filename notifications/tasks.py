from celery import shared_task

from apps.notifications.components.notification.notification_component import \
    NotificationComponent


@shared_task
def create_notification_task(name, descr, company_id, user_id, icon=None, url=None):
    NotificationComponent().create(name, descr, company_id, user_id, icon, url)
