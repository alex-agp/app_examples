from django.conf.urls import include, url

from rest_framework import routers

from apps.notifications.views import (NotificationSettingsView,
                                                NotificationsReadView,
                                                NotificationsView)

router = routers.SimpleRouter()

urlpatterns = router.urls + [
    url(r'^read/$', NotificationsReadView.as_view(), name='notifications_read'),
    url(r'^settings/$', NotificationSettingsView.as_view(), name='notifications_settings'),
    url(r'^list/$', NotificationsView.as_view(), name='notifications_list'),
]
