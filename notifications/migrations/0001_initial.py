

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('authentication', '0001_initial'),
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='NotificationSetting',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('notify_to_email', models.BooleanField(default=True)),
                ('chat_message', models.BooleanField(default=True)),
                ('chat_frequency', models.CharField(choices=[('immediately', 'Immediately'), ('every-hour', 'Every-Hour'), ('every-3h', 'Every-3H'), ('every-24h', 'Every-24H')], default='immediately', max_length=14)),
                ('tender_activity', models.BooleanField(default=True)),
                ('tender_activity_frequency', models.CharField(choices=[('immediately', 'Immediately'), ('every-hour', 'Every-Hour'), ('every-3h', 'Every-3H'), ('every-24h', 'Every-24H')], default='immediately', max_length=14)),
                ('company', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='authentication.Company')),
            ],
        ),
        migrations.CreateModel(
            name='Notifications',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=50)),
                ('description', models.TextField(null=True)),
                ('icon', models.CharField(max_length=20, null=True)),
                ('url', models.CharField(max_length=255, null=True)),
                ('date_created', models.DateTimeField(auto_now_add=True)),
                ('read', models.BooleanField(default=False)),
                ('recipient', models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, related_name='notifications', to='authentication.Company')),
                ('sender', models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, related_name='notifications', to=settings.AUTH_USER_MODEL)),
            ],
        ),
    ]
