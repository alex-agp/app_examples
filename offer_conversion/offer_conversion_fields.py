from buyer.models import (Offer, OfferConversion,
                                         OfferConversionPerUsageFee,
                                         OfferConversionVolumeBasedFee)
from common.models import Price, PricePerUnits
from supplier.models import VolumeBasedFee
from buyer.components.offers.offer_conversion.convertors.interfaces.currency_converter_interface import \
    CurrencyConverterInterface
from buyer.components.offers.offer_conversion.convertors.interfaces.unit_converter_interface import \
    UnitConverterInterface
from supplier.components.bid.bid_total_price import BidTotalPrice


class OfferConversionFields(object):
    def __init__(self, offer: Offer, currency_converter: CurrencyConverterInterface,
                 unit_converter: UnitConverterInterface):
        self.offer = offer
        self.currencyConverter = currency_converter
        self.unitConverter = unit_converter

        if self.offer.offer_conversion is None:
            self._create_offer_conversion()

    def update_all(self):
        self.save_differential()
        self.save_total_price()
        self.save_market_price()
        self.save_volume_based_fees_taxes()
        self.save_per_usage_based_fees_taxes()

        self.save_offer_cost_of_payment_terms()
        self.save_offer_cost_of_payment_total_price()

    def save_differential(self):
        differential = self.offer.bid.differential

        price, unit = self.unitConverter.convert(differential.price.value, differential.units)
        price, currency = self.currencyConverter.convert(price, differential.price.currency)

        self._update_differential(price, currency, unit)

    def save_total_price(self):
        data = BidTotalPrice().update_total_price(self.offer.bid)#  todo delete, it used for old offers to update total price
        total_price = self.offer.bid.total_price.buyer_price

        price, unit = self.unitConverter.convert(total_price.value, self.offer.round.contract.units)
        price, currency = self.currencyConverter.convert(price, total_price.currency)

        if self.offer.offer_conversion.total_price is None:
            self.offer.offer_conversion.total_price = self._create_price_per_unit(price, currency, unit)
        else:
            self._update_total_price(price, currency, unit)

        self.offer.offer_conversion.save()

    def save_market_price(self):
        market_price = self.offer.bid.market_price

        if market_price:
            price, unit = self.unitConverter.convert(market_price.price.value, market_price.units)
            price, currency = self.currencyConverter.convert(price, market_price.price.currency)

            if self.offer.offer_conversion.market_price is None:
                self.offer.offer_conversion.market_price = self._create_price_per_unit(price, currency, unit)
            else:
                self._update_market_price(price, currency, unit)
            self.offer.offer_conversion.save()

    def save_offer_cost_of_payment_terms(self):
        offer = self.offer
        payment_terms = offer.cost_of_payment_terms

        price, unit = self.unitConverter.convert(payment_terms, self.offer.round.contract.units)
        price, currency = self.currencyConverter.convert(price, self.offer.bid.total_price.buyer_price.currency)

        if self.offer.offer_conversion.cost_of_payment_terms is None:
            self.offer.offer_conversion.cost_of_payment_terms = self._create_price_per_unit(
                price, currency, unit)
        else:
            self._update_offer_cost_of_payment_terms(price, currency, unit)

        self.offer.offer_conversion.save()

    # DFTC
    def save_offer_cost_of_payment_total_price(self):
        total_price = self.offer.offer_conversion.total_price.price.value
        currency = self.offer.offer_conversion.total_price.price.currency
        unit = self.offer.offer_conversion.total_price.units

        price = self.offer.offer_conversion.cost_of_payment_terms.price.value + total_price

        if self.offer.offer_conversion.cost_of_payment_total_price is None:
            self.offer.offer_conversion.cost_of_payment_total_price = self._create_price_per_unit(price, currency, unit)
        else:
            self._update_offer_cost_of_payment_total_price(price, currency, unit)

        self.offer.offer_conversion.save()

    def _update_differential(self, price, currency, unit):
        if self.offer.offer_conversion.differential is None:
            self.offer.offer_conversion.differential = self._create_price_per_unit(price, currency, unit)
            return

        self.offer.offer_conversion.differential.price.value = price
        self.offer.offer_conversion.differential.price.currency = currency
        self.offer.offer_conversion.differential.price.save()
        self.offer.offer_conversion.differential.units = unit
        self.offer.offer_conversion.differential.save()

        self.offer.offer_conversion.save() #

    def _update_market_price(self, price, currency, unit):
        self.offer.offer_conversion.market_price.price.value = price
        self.offer.offer_conversion.market_price.price.currency = currency
        self.offer.offer_conversion.market_price.price.save()
        self.offer.offer_conversion.market_price.units = unit
        self.offer.offer_conversion.market_price.save()

    def _update_total_price(self, price, currency, unit):
        self.offer.offer_conversion.total_price.price.value = price
        self.offer.offer_conversion.total_price.price.currency = currency
        self.offer.offer_conversion.total_price.price.save()
        self.offer.offer_conversion.total_price.units = unit
        self.offer.offer_conversion.total_price.save()

    def _update_offer_cost_of_payment_total_price(self, price, currency, unit):
        self.offer.offer_conversion.cost_of_payment_total_price.price.value = price
        self.offer.offer_conversion.cost_of_payment_total_price.price.currency = currency
        self.offer.offer_conversion.cost_of_payment_total_price.price.save()
        self.offer.offer_conversion.cost_of_payment_total_price.units = unit
        self.offer.offer_conversion.cost_of_payment_total_price.save()

    def _update_offer_cost_of_payment_terms(self, price, currency, unit):
        self.offer.offer_conversion.cost_of_payment_terms.price.value = price
        self.offer.offer_conversion.cost_of_payment_terms.price.currency = currency
        self.offer.offer_conversion.cost_of_payment_terms.price.save()
        self.offer.offer_conversion.cost_of_payment_terms.units = unit
        self.offer.offer_conversion.cost_of_payment_terms.save()

    def save_volume_based_fees_taxes(self):
        bid = self.offer.bid
        volume_based_fees = bid.volume_based_fees.all()

        self._delete_volume_based_fees_taxes()

        for volume_based_fee in volume_based_fees:
            if volume_based_fee.type == VolumeBasedFee.Types.AMOUNT:
                price, unit = self.unitConverter.convert(volume_based_fee.price.price.value, volume_based_fee.price.units)
                print('--------------------')
                print(volume_based_fee.price.price.currency)
                price, currency = self.currencyConverter.convert(price, volume_based_fee.price.price.currency)

                price_per_unit = self._create_price_per_unit(price, currency, unit)

                conversion_volume_based_fee = OfferConversionVolumeBasedFee(
                    name=volume_based_fee.name, price=price_per_unit, offer_conversion=self.offer.offer_conversion
                )
                conversion_volume_based_fee.save()

    def save_per_usage_based_fees_taxes(self):
        bid = self.offer.bid
        per_usage_fees = bid.per_usage_fees.all()

        self._delete_per_usage_based_fees()

        for per_usage_fee in per_usage_fees:
            price, currency = self.currencyConverter.convert(per_usage_fee.price.value, per_usage_fee.price.currency)
            price_value = self._create_price(price, currency)

            conversion_per_usage_fee = OfferConversionPerUsageFee(
                name=per_usage_fee.name, price=price_value, offer_conversion=self.offer.offer_conversion
            )
            conversion_per_usage_fee.save()

    def _create_price_per_unit(self, price_value, currency, unit):
        price = self._create_price(price_value, currency)
        price_per_units = PricePerUnits.objects.create(**{
            'price': price,
            'units': unit
        })
        return price_per_units

    def _create_price(self, price_value, currency):
        val = round(price_value, 4)
        price = Price.objects.create(**{
            'currency': currency,
            'value': val
        })

        return price

    def _create_offer_conversion(self):
        offer_conversion = OfferConversion()
        offer_conversion.save()
        self.offer.offer_conversion = offer_conversion
        self.offer.save()

    def _delete_volume_based_fees_taxes(self):
        volumes = self.offer.offer_conversion.conversion_volume_based

        if volumes.count():
            volumes = volumes.all()
            for volume in volumes:
                if volume.price:
                    volume.price.delete()
            volumes.delete()

    def _delete_per_usage_based_fees(self):
        volumes = self.offer.offer_conversion.conversion_per_usage_fee

        if volumes.count():
            volumes = volumes.all()
            for volume in volumes:
                if volume.price:
                    volume.price.delete()
            volumes.delete()
