from buyer.components.offers.offer_conversion.offer_conversion_fields \
    import OfferConversionFields
from buyer.components.offers.offer_conversion.convertors.currency.contract_currency_converter \
    import ContractCurrencyConverter
from buyer.components.offers.offer_conversion.convertors.currency.analytic_setting_currency_converter \
    import AnalyticSettingCurrencyConverter
from buyer.components.offers.offer_conversion.convertors.unit.contract_unit_converter \
    import ContractUnitConverter
from buyer.components.offers.offer_conversion.convertors.unit.analytic_setting_unit_converter \
    import AnalyticSettingUnitConverter


class OfferConversionComponent(object):
    def __init__(self, offer):
        self.contract = offer.round.contract
        self.offer = offer

    def update(self):
        if self.contract.tender.currency.id == self.contract.currency.id:
            curr_converter = AnalyticSettingCurrencyConverter(self.contract)
        else:
            curr_converter = ContractCurrencyConverter(self.contract)

        if self.contract.tender.units.id == self.contract.units.id:
            unit_converter = AnalyticSettingUnitConverter(self.contract)
        else:
            unit_converter = ContractUnitConverter(self.contract)

        OfferConversionFields(self.offer, curr_converter, unit_converter).update_all()
