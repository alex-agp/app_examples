from common.components.currency_converter import \
    CurrencyConverter
from buyer.components.offers.offer_conversion.convertors.interfaces.currency_converter_interface import \
    CurrencyConverterInterface


class ContractCurrencyConverter(CurrencyConverterInterface):
    def __init__(self, contract):
        self.contract = contract

    def convert(self, value, currency):
        price = CurrencyConverter(self.contract.currency.code).convert(value, currency.code)
        return price, self.contract.currency
