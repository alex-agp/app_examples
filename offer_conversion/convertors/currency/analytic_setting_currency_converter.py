from buyer.components.tenders.analytic_settings.analytic_setting_currency_rates_component import \
    AnalyticSettingCurrencyRatesComponent
from buyer.components.offers.offer_conversion.convertors.interfaces.currency_converter_interface import \
    CurrencyConverterInterface


class AnalyticSettingCurrencyConverter(CurrencyConverterInterface):
    def __init__(self, contract):
        self.contract = contract

    def convert(self, value, currency):
        analytic_setting = AnalyticSettingCurrencyRatesComponent(self.contract.tender)
        price, currency = analytic_setting.convert_currency(value, currency)

        return price, currency
