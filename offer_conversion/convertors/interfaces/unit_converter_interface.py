from abc import ABC, abstractmethod


class UnitConverterInterface(object):

    @abstractmethod
    def convert(self, value, currency):
        pass
