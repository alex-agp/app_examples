from abc import ABC, abstractmethod


class CurrencyConverterInterface(object):

    @abstractmethod
    def convert(self, value, currency):
        pass
