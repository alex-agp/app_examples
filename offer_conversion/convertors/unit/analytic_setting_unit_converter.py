from common.components.units_converter.unit_converter.unit_converter import UnitsConverter

from buyer.components.offers.offer_conversion.convertors.interfaces.unit_converter_interface import \
    UnitConverterInterface


class AnalyticSettingUnitConverter(UnitConverterInterface):
    def __init__(self, contract):
        self.contract = contract
        self.unit_conversion = 331

        custom_unit_rate = self.contract.tender.analytic_setting.unit_conversion

        if custom_unit_rate:
            self.unit_conversion = float(custom_unit_rate)

    def convert(self, value, unit):
        analytic_unit = self.contract.tender.analytic_setting.units
        price = UnitsConverter(mt_usg_rate=self.unit_conversion).convert(value, analytic_unit.code, unit.code)
        return price, analytic_unit
