from common.components.units_converter.unit_converter.unit_converter import UnitsConverter

from buyer.components.offers.offer_conversion.convertors.interfaces.unit_converter_interface import \
    UnitConverterInterface


class ContractUnitConverter(UnitConverterInterface):
    def __init__(self, contract):
        self.contract = contract
        self.unit_conversion = 331

    def convert(self, value, unit):
        price = UnitsConverter(mt_usg_rate=self.unit_conversion).convert(value, self.contract.units.code, unit.code)
        return price, self.contract.units
