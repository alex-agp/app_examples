from buyer.models import Contract, Offer, Round


class ContractComponent(object):
    def __init__(self):
        pass

    def switch_next_round(self, contract, next_round):
        contract.current_round = next_round
        contract.save()
