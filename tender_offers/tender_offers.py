from buyer.models import Offer, Round


class TenderOffers(object):
    def __init__(self, tender):
        self.tender = tender

    def get_all(self):
        round_ids = self._get_tender_current_rounds()
        return Offer.objects.filter(round_id__in=round_ids)

    def get_with_received_status(self):
        round_ids = self._get_tender_current_rounds()
        return Offer.objects.filter(round_id__in=round_ids,
                                    status=Offer.Statuses.RECEIVED)

    def _get_tender_current_rounds(self):
        return [contract.current_round.id for contract in self.tender.contracts.all()]

    def _get_tender_contracts_ids(self):
        return [contract.id for contract in self.tender.contracts.all()]

    def _get_all_rounds(self):
        return Round.objects.filter(contract_id__in=self._get_tender_contracts_ids())

    def get_all_contract_rounds(self, contract):
        return Round.objects.filter(contract=contract)

    def get_with_received_status_all_rounds(self):
        round_ids = self._get_all_rounds().values('id')
        return Offer.objects.filter(round_id__in=round_ids, status=Offer.Statuses.RECEIVED)

    def get_with_decline_submitted_all_contract_rounds(self, contract):
        round_ids = self.get_all_contract_rounds(contract).values('id')
        return Offer.objects.filter(round_id__in=round_ids,
                                    status__in=[Offer.Statuses.DECLINED_SUBMITTED, Offer.Statuses.RECEIVED])

    def get_with_decline_submitted_unique_supplier_offers(self, contract):
        offers = self.get_with_decline_submitted_all_contract_rounds(contract).order_by('supplier_id').distinct(
            'supplier_id')
        # suppliers_ids = list(set(offers.values('supplier_id')))
        return offers

    def get_supplier_rounds_feedbacks(self, offer):
        feedbacks = []

        rounds = self.get_all_contract_rounds(offer.round.contract)

        for round in rounds:
            supplier_offers = round.offers.filter(
                status__in=[Offer.Statuses.DECLINED_SUBMITTED, Offer.Statuses.RECEIVED, Offer.Statuses.AWARDED],
                supplier=offer.supplier)

            for offer in supplier_offers:
                if offer.bid.feedback:
                    feedbacks.append(offer.bid.feedback)

        return feedbacks
