from apps.common.components.units_converter.units_weights.units_weights_rates import \
    UnitsWeightsRates
from django.test import TestCase


class UnitsWeightsRatesTest(TestCase):

    def setUp(self):
        self.units_weights_converter = UnitsWeightsRates()

    def test_get_rates_is_correct(self):
        self.assertEqual(self.units_weights_converter.get_rates('KG')['MT'], 0.001)

    def test_get_rate_is_correct(self):
        self.assertEqual(self.units_weights_converter.get_rate('MT', 'KG'), 1000)
        self.assertEqual(self.units_weights_converter.get_rate('LBS', 'KG'), 0.45359237)

    def test_is_unit_name_not_correct(self):
        with self.assertRaises(Exception):
            self.units_weights_converter._is_unit_exist('MTerr')

    def test_is_unit_exist(self):
        self.assertTrue(self.units_weights_converter.is_unit_exist('KG'))


