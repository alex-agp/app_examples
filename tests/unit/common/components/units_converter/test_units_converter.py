from apps.common.components.units_converter.unit_converter.unit_converter import \
    UnitsConverter
from django.test import TestCase


class UnitsConverterTest(TestCase):

    def setUp(self):
        pass

    def test_convert_unit_not_correct(self):
        with self.assertRaises(ValueError):
            UnitsConverter().convert('1', 'MTerr', 'BBL')

    def test_convert_is_correct_when_same_units(self):
        units_converter = UnitsConverter()
        value = units_converter.convert(179, 'LTR', 'LTR')
        self.assertEqual(value, 179)

    def test_convert_volume_to_volume_is_correct(self):
        units_converter = UnitsConverter()
        value1 = units_converter.convert(1, 'BBL', 'LTR')
        value2 = units_converter.convert(200, 'LTR', 'BBL')
        value3 = units_converter.convert(100, 'USG', 'BBL')
        value4 = units_converter.convert(1, 'LTR', 'USG')
        value5 = units_converter.convert(1, 'USG', 'LTR')

        self.assertEqual(value1, 158.987295)
        self.assertEqual(value2, 1.257962)
        self.assertEqual(value3, 2.380952)
        self.assertEqual(value4, 0.264172)
        self.assertEqual(value5, 3.785412)

    def test_convert_weight_to_weight_is_correct(self):
        units_converter = UnitsConverter()
        value1 = units_converter.convert(1, 'MT', 'KG')
        value2 = units_converter.convert(1, 'KG', 'MT')
        value3 = units_converter.convert(1, 'LBS', 'MT')
        value4 = units_converter.convert(1, 'MT', 'LBS')
        value5 = units_converter.convert(1, 'KG', 'LBS')

        self.assertEqual(value1, 1000)
        self.assertEqual(value2, 0.001)
        self.assertEqual(value3, 0.000454)
        self.assertEqual(value4, 2204.62)
        self.assertEqual(value5, 2.204623)

    def test_convert_weight_to_volume_is_correct(self):
        units_converter = UnitsConverter()
        value1 = units_converter.convert(1, 'KG', 'LTR')
        value2 = units_converter.convert(1, 'MT', 'LTR')
        value3 = units_converter.convert(100, 'LBS', 'BBL')

        self.assertEqual(value1, 1.252971)
        self.assertEqual(value2, 1252.971301)
        self.assertEqual(value3, 0.357472)

    def test_convert_volume_to_weight_is_correct(self):
        units_converter = UnitsConverter()
        value1 = units_converter.convert(1, 'LTR', 'KG')
        value2 = units_converter.convert(1, 'LTR', 'MT')
        value3 = units_converter.convert(100, 'BBL', 'LBS')
        value4 = units_converter.convert(1, 'BBL', 'MT')

        self.assertEqual(value1, 0.798)
        self.assertEqual(value2, 0.000798)
        self.assertEqual(value3, 27974.030758)
        self.assertEqual(value4, 0.126888)
