from apps.common.components.units_converter.units_volumes.units_volumes_rates import \
    UnitsVolumesRates
from django.test import TestCase


class UnitsVolumesRatesTest(TestCase):

    def setUp(self):
        self.units_volumes_converter = UnitsVolumesRates()

    def test_is_unit_not_correct(self):
        with self.assertRaises(Exception):
            self.units_volumes_converter.get_rate('BBL', 'USG1')

    def test_is_rates__bbl_to_usg_correct(self):
        rate = self.units_volumes_converter.get_rates('BBL')
        self.assertEqual(rate['USG'], 42)

    def test_is_rates_correct(self):
        rate1 = self.units_volumes_converter.get_rate('BBL', 'USG')
        rate2 = self.units_volumes_converter.get_rate('CBM', 'LTR')
        self.assertEqual(rate1, 42)
        self.assertEqual(rate2, 1000)

    def test_is_unit_exist(self):
        self.assertTrue(self.units_volumes_converter.is_unit_exist('BBL'))

