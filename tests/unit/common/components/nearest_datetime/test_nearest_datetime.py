from datetime import datetime, timezone
from django.test import TestCase
from tests.utilities.testing_utilities import TestingUtilities
from apps.common.components.nearest_datetime.nearest_datetime import NearestDateTime


class NearestDatetimeTest(TestCase):

    def setUp(self):
        self.testing_utilities = TestingUtilities()
        self.testing_utilities.populate_test_db()

        self.date_time = datetime(2019, 5, 18, 16, 19, tzinfo=timezone.utc)
        self.nearest_date_time = NearestDateTime(self.date_time)

    def test_hour_is_correct(self):
        date_time_test = datetime(2019, 5, 18, 17, 0, tzinfo=timezone.utc)
        self.assertEqual(self.nearest_date_time.get_hour(), date_time_test)

    def test_hour_is_not_correct(self):
        date_time_test = datetime(2019, 4, 11, 17, 0, tzinfo=timezone.utc)
        self.assertNotEquals(self.nearest_date_time.get_hour(), date_time_test)

    def test_3th_hour_is_correct(self): # 00,03,06,09,12,15,18,21
        date_time_test = datetime(2019, 5, 18, 18, 0, tzinfo=timezone.utc)
        self.assertEqual(self.nearest_date_time.get_3th_hour(), date_time_test)

    def test_24th_hour_is_correct(self):
        date_time_test = datetime(2019, 5, 19, 5, 0, tzinfo=timezone.utc)
        self.assertEqual(self.nearest_date_time.get_24th_hour(), date_time_test)

    def test_24th_hour_is_not_correct(self):
        date_time_test = datetime(2018, 9, 19, 5, 0, tzinfo=timezone.utc)
        self.assertNotEquals(self.nearest_date_time.get_24th_hour(), date_time_test)

    def test_next_minute_is_correct(self):
        date_time_test = datetime(2019, 5, 18, 16, 20, tzinfo=timezone.utc)
        self.assertEqual(self.nearest_date_time.get_next_minute(), date_time_test)

    def test_numbers_3th_hour_is_correct(self):
        self.assertEqual(self.nearest_date_time.get_number_3th_hour(1), 3)
        self.assertEqual(self.nearest_date_time.get_number_3th_hour(5), 6)
        self.assertEqual(self.nearest_date_time.get_number_3th_hour(8), 9)
        self.assertEqual(self.nearest_date_time.get_number_3th_hour(10), 12)
        self.assertEqual(self.nearest_date_time.get_number_3th_hour(14), 15)
        self.assertEqual(self.nearest_date_time.get_number_3th_hour(17), 18)
        self.assertEqual(self.nearest_date_time.get_number_3th_hour(20), 21)
        self.assertEqual(self.nearest_date_time.get_number_3th_hour(22), 1)
