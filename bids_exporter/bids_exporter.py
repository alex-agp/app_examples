import datetime
from buyer.models import Offer, Round
from common.exporters.data_formatters.excel_data_formatter import \
    ExcelDataFormatter
from common.exporters.serializers.bids_exporter_serializer import BidsExporterSerializer

# Field mapping & order
FIELD_MAPPING = {
    'Offer ID': 'name',
    'ICAO': 'icao',
    'IATA': 'iata',
    'Bid Round': 'bid_round',
    'Supplier': 'supplier',
    'Status': 'status',
    'Delivery point': 'delivery_point',
    'Prices on': 'prices_on',
    'Price Structure 1': 'price_structure_1',
    'Price Structure 2': 'price_structure_2',
    'Price Structure 3': 'price_structure_3',
    'Price Structure 4': 'price_structure_4',
    'Price Structure 5': 'price_structure_5',
    'Payment Currency': 'payment_currency',
    'Payment Unit': 'payment_unit',
    'Payment Method': 'payment_method',
    'Payment Deposits': 'payment_deposit',
    'Exchange Rate Source': 'exchange_rate_source',
    'Exchange Rate Averaging Method': 'exchange_rate_method',
    'Exchange Rate Averaging offset': 'exchange_rate_offset',
    'Differential value': 'differential',
    'Differential currency': 'differential_currency',
    'Differential units': 'differential_unit',
    'Per usage based fees and taxes': 'usage_based_fees',
    'Volume Based Mandatory Fees & Taxes': 'volume_based_fees_taxes',
    'DFT value': 'total_price',
    'DFT currency': 'total_price_currency',
    'DFT units': 'total_price_units',
    'Comparative Cost Of Payment Terms': 'cost_of_payment_terms',
    'Comparative Cost Of Payment Terms Value': 'cost_of_payment_total_price',
    'Delivery methods to the airport': 'delivery_to_airport',
    'Delivery method to the aircraft': 'delivery_to_aircraft',
    'Non-mandatory fees': 'non_mandatory_fees',
    'Intoplane agent': 'intoplane_agent',
    'Fuel Providing': 'fuel_providing',
    # 'Payment terms': 'payment_terms',
    # 'Invoice frequency': 'invoice_frequency',
    'Payment guaranty value': 'payment_guaranty',
    'Payment guaranty currency': 'payment_guaranty_currency',
    'Open fuel release': 'open_fuel_release',
    'Pre-flight notification hours': 'pre_flight_notification',
    'Pre-flight notification notes': 'pre_flight_notification_notes',
    'Ramp limitations': 'ramp_limitations',
    'Fuel availability': 'fuel_availability',
    # 'Payment Methods': 'payment_methods',
    'Billing Currency': 'billing_currency',
    'Gross Or Net Billing': 'grossnet_billing',
    'Notes': 'notes',
    'Bid Notes': 'bid_notes',
    'Prepayment': 'prepayment',
    'Number Of Days Prepaid': 'number_days_prepaid',
    'Payment First Delivery Date': 'payment_first_delivery_date',
    'Payment Frequency': 'payment_frequency',
    'Amount': 'amount',
    'Open Invoice': 'open_invoice',
    'Payment terms': 'payment_terms',
    'Payment reference date': 'payment_reference_date',
    'Invoice frequency': 'invoice_frequency',
    'Invoice type': 'invoice_type',
}


class BidsExporter:
    def __init__(self, tender, contract=None, company=None, seller=False, order=None):
        self.company = company
        self.tender = tender
        self.contract = contract
        self.seller = seller
        # TODO: Add sort order implementation
        self.order = order

    def export(self, data_formatter=ExcelDataFormatter):
        if self.contract is None:
            contracts = self.tender.contracts.all()
            rounds = Round.objects.filter(contract__in=contracts)
        else:
            rounds = Round.objects.filter(contract=self.contract)

        offers = Offer.objects.filter(round__in=rounds, bid__isnull=False, status__in=[Offer.Statuses.RECEIVED,
            Offer.Statuses.AWARDED, Offer.Statuses.DECLINED_SUBMITTED])

        if self.company is not None:
            offers = offers.filter(supplier=self.company)

        offers = offers.select_related('round', 'bid', 'bid__fuel_preference', 'supplier', 'round__contract',
           'round__contract__location', 'bid__platts_index_price', 'bid__argus_index_price',
           'bid__platts_index_price__index', 'bid__argus_index_price__index', 'bid__platts_index_price__bates',
           'bid__argus_index_price__price_type', 'bid__argus_index_price__index__differential_basis',
           'bid__platts_index_price__index__mdc', 'bid__argus_index_price__index__frequency', 'bid__market_price__price',
           'bid__market_price', 'bid__platts_index_price__index__currency', 'bid__argus_index_price__index__currency',
           'bid__platts_index_price__time_period', 'bid__platts_index_price__averaging_offset',
           'bid__argus_index_price__time_period', 'bid__argus_index_price__averaging_offset',
           'bid__market_price__price__currency', 'bid__platts_index_price__index__units', 'bid__argus_index_price__index__units',
           'bid__market_price__units', 'bid__total_price', 'bid__total_price__buyer_price', 'bid__market_price_source_type',
           'bid__total_price__buyer_price__currency', 'bid__differential', 'bid__differential__price',
           'bid__differential__price__currency', 'bid__differential__units', 'bid__base_price',
           'bid__base_price__buyer_price', 'bid__base_price__buyer_price__currency', 'bid__payment',
           'bid__payment__open_invoice', 'bid__aircraft_delivery_method', 'bid__payment__unit', 'bid__payment__currency',
           'bid__payment__prepayment', 'bid__payment__prepayment__payment_frequency',
           'bid__payment__method_of_payment', 'bid__payment__exchange_rate', 'bid__payment__exchange_rate__financial_source',
           'bid__payment__exchange_rate__averageing_method', 'bid__payment__exchange_rate__averaging_offset',
           'bid__payment__open_invoice__payment_reference_date', 'bid__payment__open_invoice__invoice_frequency',
           'bid__payment__open_invoice__invoice_type', 'bid__pre_flight_notification', 'bid__ramp_limitations',
           'bid__fuel_availability', 'bid__gross_net_billing')\
            .prefetch_related('bid__per_usage_fees', 'bid__per_usage_fees__price',
            'bid__per_usage_fees__price__currency',
            'bid__volume_based_fees', 'bid__volume_based_fees__price', 'bid__volume_based_fees__price__units',
            'bid__volume_based_fees__price__price', 'bid__volume_based_fees__price__price__currency',
            'bid__airport_delivery_methods', 'bid__airport_delivery_methods__method',
            'bid__non_mandatory_fees', 'bid__non_mandatory_fees__price', 'bid__non_mandatory_fees__currency')

        serializer = BidsExporterSerializer(tender=self.tender, offers=offers)

        formatter = data_formatter(serializer.data, FIELD_MAPPING)

        return formatter.generate()
